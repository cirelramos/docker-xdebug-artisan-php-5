#!/bin/bash
echo "${ENV_MYSQL_MULTIPLE_DATABASE}"
variable="${ENV_MYSQL_MULTIPLE_DATABASE}"
echo "" > /docker-entrypoint-initdb.d/init.sql.sql
for i in $(echo $variable | sed "s/,/ /g")
do
echo "CREATE  DATABASE IF NOT EXISTS \`$i\`;" >> /docker-entrypoint-initdb.d/init.sql.sql
echo "use \`$i\`; " >> /docker-entrypoint-initdb.d/init.sql.sql
echo "GRANT ALL ON \`$i\` TO 'root'@'%';" >> /docker-entrypoint-initdb.d/init.sql.sql
echo "FLUSH PRIVILEGES ;" >> /docker-entrypoint-initdb.d/init.sql.sql
done
cat /docker-entrypoint-initdb.d/init.sql.sql

