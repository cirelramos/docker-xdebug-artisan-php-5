#!/bin/bash
echo $HRROLE
case "$HRROLE" in
  "backend")
    bash -sic "cd /var/www/ && composer install && nohup /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord-default.conf"
    ;;
  "backend-horizon")
    bash -sic "cd /var/www/ && composer install && nohup /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord-default.conf"
    ;;
  *)
    echo "No especifico el HRROLE"
    ;;
esac
